//
//  ViewController.swift
//  CoreDataGr2
//
//  Created by alexfb on 14/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var apellidoTextField: UITextField!
    @IBOutlet weak var telefonoTextField: UITextField!
    @IBOutlet weak var direccionTextField: UITextField!
    
    let managedObjectContext=(UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveButton(_ sender: Any) {
        let entityDescription=NSEntityDescription.entity(forEntityName: "Estudiante", in: managedObjectContext)
        let estudiante=Estudiante(entity: entityDescription!, insertInto: managedObjectContext)
        
        
        estudiante.nombre=nombreTextField.text!
        estudiante.apellido=apellidoTextField.text!
        estudiante.telefono=telefonoTextField.text!
        estudiante.direccion=direccionTextField.text!
        
        
        do{
        try managedObjectContext.save()
        }catch let error{
            print(error)
        }
    }

    @IBAction func listAction(_ sender: Any) {
        let entityDescription=NSEntityDescription.entity(forEntityName: "Estudiante", in: managedObjectContext)
        let request:NSFetchRequest<Estudiante>=Estudiante.fetchRequest()
        request.entity=entityDescription!
        
        do{
            let results=try managedObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            for obj in results{
               let est=obj as! Estudiante
                print("nombre: \(est.nombre!), apellido: \(est.apellido!)")
                
            }
        }catch let error{
            print(error)
        }
    }
    
    
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let entityDescription=NSEntityDescription.entity(forEntityName: "Estudiante", in: managedObjectContext)
        let request:NSFetchRequest<Estudiante>=Estudiante.fetchRequest()
        request.entity=entityDescription!
        let predicate=NSPredicate(format: "nombre = %@", nombreTextField.text!)
        request.predicate=predicate
        do{
            let results=try managedObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            guard let est=results.first as? Estudiante else{
                return
            }
            apellidoTextField.text=est.apellido!
            telefonoTextField.text=est.telefono!
            direccionTextField.text=est.direccion!
        }catch let error{
            print(error)
        }
    }
}

